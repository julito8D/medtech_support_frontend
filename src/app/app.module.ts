import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing-module";
import { HomeComponent } from "./components/home/home.component";
import { NavComponent } from "./components/nav/nav.component";
import { NewsComponent } from "./components/home/news/news.component";
import { DocoverviewComponent } from "./components/docoverview/docoverview.component";
import { DocselectedComponent } from "./components/docoverview/docselected/docselected.component";
import { InfographicComponent } from "./components/home/infographic/infographic.component";
import { FaqComponent } from "./components/home/faq/faq.component";
import { FooterComponent } from "./components/footer/footer.component";
import { CommentComponent } from "./components/docoverview/docselected/docsidebar/docsidebar.component";
import { ContactbuttonComponent } from "./components/contactbutton/contactbutton.component";

import { FormsModule } from "@angular/forms";

import { RestService } from "./service/rest.service";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { SanitizeHtmlPipe } from "./pipes/sanitize.pipe";


@NgModule({
  declarations: [
    AppComponent,
    // Components
    HomeComponent,
    NavComponent,
    NewsComponent,
    DocoverviewComponent,
    DocselectedComponent,
    InfographicComponent,
    FaqComponent,
    FooterComponent,
    CommentComponent,
    ContactbuttonComponent,

    // Pipes
    SanitizeHtmlPipe,
  ],
  imports: [
    BrowserModule,
    //HTTP Requests
    HttpClientModule,
    // routing
    AppRoutingModule,
    BrowserAnimationsModule,

    FormsModule,
  ],
  providers: [
    RestService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
