import { Injectable } from "@angular/core";
import { CommentModel } from "../model/comment.model";
import { DocumentModel } from "../model/document.model";
import { RestService } from "./rest.service";

@Injectable({ providedIn: "root" })
export class DocumentService {
  constructor(private restService: RestService) {}

  private COMMENT_URL = this.restService.BASE_URL + "comments";
  private DOCUMENTS_URL = this.restService.BASE_URL + "documents";

  getComment(id: string) {
    return this.restService.getGeneric<CommentModel>(
      this.COMMENT_URL + "/" + id,
    );
  }

  getComments() {
    return this.restService.getAllGeneric<CommentModel>(this.COMMENT_URL);
  }

  addComment(comment: CommentModel) {
    return this.restService.postGeneric<CommentModel>(
      this.COMMENT_URL,
      comment,
    );
  }

  getDocuments() {
    return this.restService.getGeneric<DocumentModel[]>(
      this.DOCUMENTS_URL + "/",
    );
  }

  getDocumentsById(id: string) {
    return this.restService.getGeneric<DocumentModel[]>(
      this.DOCUMENTS_URL + "/" + id,
    );
  }

  getDocumentByIdVersion(id: string, version: string) {
    return this.restService.getGeneric<DocumentModel>(
      this.DOCUMENTS_URL + "/" + id + "/" + version,
    );
  }
}
