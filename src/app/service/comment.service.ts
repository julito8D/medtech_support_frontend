import { Injectable } from "@angular/core";
import { PostCommentDto } from "../dto/post.comment.dto";
import { CommentModel } from "../model/comment.model";
import { RestService } from "./rest.service";

@Injectable({ providedIn: "root" })
export class CommentService {
  constructor(private restService: RestService) {}

  private COMMENT_URL = this.restService.BASE_URL + "comments";

  getComment(id: string) {
    return this.restService.getGeneric<CommentModel>(
      this.COMMENT_URL + "/" + id,
    );
  }

  getComments() {
    return this.restService.getAllGeneric<CommentModel>(this.COMMENT_URL);
  }

  getCommentsByDocIdAndVersionDate(docId: string, versionDate: Date) {
    return this.restService.getGeneric<CommentModel[]>(
      this.COMMENT_URL + "/" + docId + "/" + versionDate,
    );
  }

  addComment(comment: PostCommentDto) {
    return this.restService.postGeneric<CommentModel>(
      this.COMMENT_URL,
      comment,
    );
  }
}
