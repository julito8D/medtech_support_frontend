import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from "@angular/common/http";
import { catchError } from "rxjs/operators";
import { throwError } from "rxjs";
import { environment } from "../../environments/environment";

/*
  * Service for REST requests
*/
@Injectable()
export class RestService {
  public BASE_URL = environment.apiUrl;

  constructor(private httpClient: HttpClient) {
  }

  getGeneric<T>(url: string) {
    return this.httpClient.get<T>(url, { headers: this.getHeaders() })
      .pipe(catchError(this.handleError));
  }

  getAllGeneric<T>(url: string) {
    return this.httpClient.get<T[]>(url, { headers: this.getHeaders() })
      .pipe(catchError(this.handleError));
  }

  postGeneric<T>(url: string, entity: any) {
    return this.httpClient.post<T>(url, JSON.stringify(entity), {
      headers: this.getHeaders(),
    })
      .pipe(catchError(this.handleError));
  }

  putGeneric<T>(url: string, entity: any) {
    return this.httpClient.put<T>(url, JSON.stringify(entity), {
      headers: this.getHeaders(),
    })
      .pipe(catchError(this.handleError));
  }

  deleteGeneric(url: string) {
    return this.httpClient.delete(url, { headers: this.getHeaders() })
      .pipe(catchError(this.handleError));
  }

  private getHeaders() {
    const headers = new HttpHeaders()
      .set("Content-Type", "application/json");
    return headers;
  }

  private handleError(errorResponse: HttpErrorResponse) {
    const messageError = "Unexpected error";
    return throwError(messageError);
  }
}
