import { Component, Input, OnChanges, OnInit, SimpleChanges } from "@angular/core";
import { DOCUMENT } from "@angular/common";
//import { RestService } from "../../../../service/rest.service";
import { CommentModel } from "../../../../model/comment.model";
import { NgForm } from "@angular/forms";
import { DocumentService } from "src/app/service/document.service";
import { PostCommentDto } from "src/app/dto/post.comment.dto";
import { CommentService } from "src/app/service/comment.service";
import { DocumentModel } from "src/app/model/document.model";

@Component({
  selector: "app-docsidebar",
  templateUrl: "./docsidebar.component.html",
  styleUrls: ["docsidebar.component.css"],
})
export class CommentComponent implements OnInit, OnChanges {
  comments: CommentModel[] = [];

  @Input()
  relatedDocument?: DocumentModel;

  comment: PostCommentDto = new PostCommentDto(
    "",
    "",
    "",
    new Date(),
  );

  constructor(private commentService: CommentService) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.fetchComments();
  }

  save() {
    let postComment = new PostCommentDto(
      this.comment.name,
      this.comment.comment,
      this.relatedDocument?.versionedDocument.id,
      this.relatedDocument?.versionedDocument.versionDate,
    );

    this.commentService.addComment(postComment)
      .subscribe((data: CommentModel) => {
        this.comments.push(data);
      });
  }

  clear() {
  }

  private fetchComments() {
    if (this.relatedDocument) {
      this.commentService.getCommentsByDocIdAndVersionDate(
        this.relatedDocument?.versionedDocument.id,
        this.relatedDocument?.versionedDocument.versionDate,
      )
        .subscribe((data: CommentModel[]) => {
          this.comments = data;
          console.log(this.comments);
        });
    } else {
      console.log("Related doc is null!");
    }
  }
}
