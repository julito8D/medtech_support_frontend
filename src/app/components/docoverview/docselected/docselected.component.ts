import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { RestService } from "../../../service/rest.service";
import { Node } from "../../../model/node.model";
import { DocumentModel } from "../../../model/document.model";
import { ActivatedRoute } from "@angular/router";
import { ViewportScroller } from "@angular/common";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { DocumentService } from "src/app/service/document.service";

@Component({
  selector: "app-docselected",
  templateUrl: "./docselected.component.html",
  styleUrls: ["./docselected.component.css"],
  encapsulation: ViewEncapsulation.None,
})
export class DocselectedComponent implements OnInit {
  documents: DocumentModel[] | undefined;

  id: string = "";

  selectedDocument: DocumentModel | undefined;

  dates: Date[] = [];
  selectedDate: Date = new Date();

  showChangedChecked: boolean = false;

  constructor(
    private documentService: DocumentService,
    private route: ActivatedRoute,
    private viewPortScroller: ViewportScroller,
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((paramMap) => {
      const id = paramMap.get("id");

      if (id) {
        this.id = id;
        this.documentService.getDocumentsById(this.id)
          .subscribe((data: DocumentModel[]) => {
            this.documents = data;
            this.selectedDocument = this.documents[this.documents.length - 1];
            console.log("Document fetched!");

            this.dates = this.documents.map((doc: DocumentModel) =>
              doc.versionedDocument.versionDate
            );
            this.selectedDate = this.dates[this.dates.length - 1];
          });
      }
    });
  }

  public onClick(elementId: string): void {
    this.viewPortScroller.scrollToAnchor(elementId);
  }

  public onChange() {
    console.log(this.selectedDate);
    this.documentService.getDocumentByIdVersion(
      this.id,
      this.selectedDate.toString(),
    )
      .subscribe((data: DocumentModel) => {
        this.selectedDocument = data;
        console.log(this.selectedDocument);
      });
  }

  public onChecked() {
    if (this.showChangedChecked) {
      this.documentService.getDocumentByIdVersion(
        this.id + "-div",
        this.selectedDate.toString(),
      )
        .subscribe((data: DocumentModel) => {
          this.selectedDocument = data;
          console.log(this.selectedDocument);
        });
    } else {
      this.documentService.getDocumentByIdVersion(
        this.id,
        this.selectedDate.toString(),
      )
        .subscribe((data: DocumentModel) => {
          this.selectedDocument = data;
          console.log(this.selectedDocument);
        });
    }
  }
}
