import { Component } from "@angular/core";
import { RestService } from "../../service/rest.service";
import { CommentModel } from "../../model/comment.model";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["home.component.css"],
})
export class HomeComponent {
  //CommentModel
  usercomments: CommentModel[] = [];

  constructor(private restService: RestService) {
  }
}
