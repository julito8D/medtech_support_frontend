import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

// {{title}} displays the value of title in "app.components.ts"
export class AppComponent {



  // //Testing 2
  // constructor (private router:Router) {};

  // Listar() {
  //   this.router.navigate(["listar"]); //listar has to be the same name as the path value in app-routing-module
  // }

  // Nuevo() {
  //   this.router.navigate(["add"]);
  // }
  
}


