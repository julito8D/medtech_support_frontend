import { ArticleModel } from './article.model';


//TabSubTwoCh
export class SectionModel {
    constructor (
        public id: string,
        public title: string,
        public tabSubOneChEntity: string,
        public tabSubThreeChEntities: ArticleModel[]
    ) {}
}