import { ArticleModel } from "./article.model";
import { SectionModel } from "./section.model";



//TabSubOneCh
export class ChapterModel {
    constructor (
        public id: string,
        public title: string,
        public tabSubTwoChEntities: SectionModel[],
        public tabSubThreeChEntities: ArticleModel [],
    ) {}
}