import { ChapterModel } from "./chapter.model";
import { VersionedDocument } from "./versioneddocument";

//TabDocCh
export class DocumentModel {
  constructor(
    public versionedDocument: VersionedDocument = new VersionedDocument(),
    public titleLong: string = "",
    public titleShort: string = "",
    public content: string = "",
    public tabSubOneChEntities: ChapterModel[],
  ) {}
}
