
export class Node {
    constructor (
        public id: string = "",
        public title: string = "",
        public children: Node[] = []
    ) {}
}



// export class Node {
//     constructor(
//         public id: number = 0,
//         public gesetzkuerzel: string = "",
//         public veroeffentlichung: Date = new Date(),
//         public texteinleitung: string = "",
//         public children: Node[] = []
//     ) {}

// }