export class VersionedDocument {
    constructor(
      public id: string = "",
      public versionDate: Date = new Date(),
    ) {
    }
  }
  