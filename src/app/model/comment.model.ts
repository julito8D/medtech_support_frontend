export class CommentModel {
    constructor(
      public id: string = "",
      public name: string = "",
      public comment: string = "",
      public currentDate: Date = new Date(),
      
    ) {}
  }
  