//TabSubThreeCh
export class ArticleModel {
    constructor (
        public id: string,
        public title: string,
        public content: string,
        public tabSubTwoChEntity: string,
        public tabSubOneChEntity: string,
    ) {}
}