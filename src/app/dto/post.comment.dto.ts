export class PostCommentDto {
    constructor(
      public name: string = "",
      public comment: string = "",
      public docId: string = "",
      public docVersionDate: Date = new Date(),
    ) {}
  }