import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { DocoverviewComponent } from "./components/docoverview/docoverview.component";

import { DocselectedComponent } from "./components/docoverview/docselected/docselected.component";

const appRoutes: Routes = [
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: "home", component: HomeComponent },
  { path: "docoverview", component: DocoverviewComponent },

  { path: "docoverview/docselected/:id", component: DocselectedComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
